from gae2django.gaeapi.appengine.ext import db
from codereview import models, views
from codereview import engine  # engine must be imported after models :(

from .utils import TestCase, load_file, MockRequest

class TestPublish(TestCase):

    def setUp(self):
        super(TestPublish, self).setUp()
        self.issue = models.Issue(subject='test')
        self.issue.local_base = False
        self.issue.owner_id = self.user.pk
        self.issue.put()
        self.ps = models.PatchSet(parent=self.issue, issue=self.issue)
        data = load_file('ps1.diff')
        self.ps.data = data
        self.ps.save()
        self.patches = engine.ParsePatchSet(self.ps)
        db.put(self.patches)

    def test_draft_details_no_base_file(self):
        request = MockRequest(self.user, issue=self.issue)
        cmt = models.Comment(patch=self.patches[0], parent=self.patches[0])
        cmt.text = 'test comment'
        cmt.lineno = 1
        cmt.left = False
        cmt.draft = True
        cmt.author = self.user
        cmt.save()
        tbd, comments = views._get_draft_comments(request, self.issue)
        self.assertEqual(len(comments), 1)
        # Try to render draft details:
        views._get_draft_details(request, comments)
