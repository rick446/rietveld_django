import os
import unittest

from django.http import HttpRequest
from django.test.client import Client
from django.contrib.auth.models import User

FILES_DIR = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'files')

class TestCase(unittest.TestCase):

    def setUp(self):
        self.user = User.objects.create_user('foo', 'foo@example.com', 'foopassword')
        self.user.save()
        self.client = Client()

    def tearDown(self):
        self.user.delete()

    def login(self, username, password):
        self.client.login(username=username, password=password)

    def logout(self):
        self.client.logout()

class MockRequest(HttpRequest):
    """Mock request class for testing."""

    def __init__(self, user=None, issue=None):
        super(MockRequest, self).__init__()
        self.META['HTTP_HOST'] = 'testserver'
        self.user = user
        self.issue = issue

def load_file(fname):
  """Read file and return it's content."""
  return open(os.path.join(FILES_DIR, fname)).read()
        
